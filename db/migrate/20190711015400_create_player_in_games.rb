class CreatePlayerInGames < ActiveRecord::Migration[5.2]
  def change
    create_table :player_in_games do |t|
      t.references :player, foreign_key: true
      t.references :game, foreign_key: true
      t.integer :kills, :default => 0

      t.timestamps
    end
  end
end
