class CreateCauseOfDeaths < ActiveRecord::Migration[5.2]
  def change
    create_table :cause_of_deaths do |t|
      t.references :game, foreign_key: true
      t.string :cause_of_death_by
      t.integer :count, :default => 0

      t.timestamps
    end
  end
end
