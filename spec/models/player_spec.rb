require 'rails_helper'

RSpec.describe Player, type: :model do
  
  before(:each) do
    @player = FactoryBot.create(:player)
  end

  describe 'Associations' do
    it { should have_many :player_in_games }
  end

  describe 'Validations' do
    context 'name' do
        it 'should be unique' do
            user_repeated = FactoryBot.create(:player)
            user_repeated.name = @player.name
            expect(user_repeated).to_not be_valid
        end

        it 'should not be blank' do
            @player.name = nil
            expect(@player).to_not be_valid
        end
    end
  end 



end
