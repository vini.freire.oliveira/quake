require 'rails_helper'

RSpec.describe CauseOfDeath, type: :model do
  
  describe 'Associations' do
    it { should belong_to :game }
  end

  describe 'Validations' do
    context 'cause_of_death_by' do
        it 'should not be blank' do
            cause_of_death = FactoryBot.create(:cause_of_death)
            cause_of_death.cause_of_death_by = nil
            expect(cause_of_death).to_not be_valid
        end
    end
    
  end 


end
