require 'rails_helper'

RSpec.describe Game, type: :model do
  
  describe 'Associations' do
    it { should have_many :player_in_games }
    it { should have_many :cause_of_deaths }
    it { should have_many :players }
  end

end
