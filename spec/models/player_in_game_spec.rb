require 'rails_helper'

RSpec.describe PlayerInGame, type: :model do
  describe 'Associations' do
    it { should belong_to :player }
    it { should belong_to :game }
  end
end
