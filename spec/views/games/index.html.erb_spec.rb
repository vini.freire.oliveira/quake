require 'rails_helper'


RSpec.describe "Upload File Process", :type => :system, js: true do
    
    it "upload test log file" do
        visit '/'
        within("form") do
            find('#log', visible: false).set File.absolute_path('./qgames.log')
        end

        # save_and_open_page
        expect(page).to have_link
        expect(page).to have_content("Ruby Developer position")
        #expect(page).to have_css('.card', text: '...')
    end
end
