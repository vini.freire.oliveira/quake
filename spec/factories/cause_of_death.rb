FactoryBot.define do
    factory :cause_of_death do
        game { FactoryBot.create(:game) }
        cause_of_death_by { FFaker::NameBR.unique.name }
    end
end
