FactoryBot.define do
    factory :game do
        kills { rand(1..100) }
    end
end
