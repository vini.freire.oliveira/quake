FactoryBot.define do
    factory :player do
        name { FFaker::NameBR.unique.name }
    end
end
