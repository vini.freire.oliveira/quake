require 'rails_helper'
 
RSpec.describe "Game", type: :request do

    before do
        post "/parsers", params: { parser: {log: Rack::Test::UploadedFile.new("#{Rails.root}/public/qgames.log", 'qgames/log')} }
    end
    
    describe "Get /index" do
        
        before do
            get "/games.json"
        end

        it "returns ok" do
            expect(response.status).to eql(200)
        end

        it "return json with one game" do
            expect(JSON.parse(response.body).count).to eql(1)
        end

        it "json has to have the elements" do
            parsed_response = JSON.parse(response.body)            
            expect(parsed_response[0]["id"]).not_to be_nil
            expect(parsed_response[0]["kills"]).not_to be_nil
            expect(parsed_response[0]["players"].count).to eql(4)
            expect(parsed_response[0]["kills_by_means"].count).to eql(7)
        end

    end

    describe "Get /show" do
        
        before do
            @game = Game.last
            get "/games/#{@game.id}.json"
        end

        it "returns ok" do
            expect(response.status).to eql(200)
        end

        it "json has to have the elements" do
            parsed_response = JSON.parse(response.body)   
            expect(parsed_response["id"]).to eql(@game.id)
            expect(parsed_response["kills"]).to eql(@game.kills)
            expect(parsed_response["players"].count).to eql(@game.players.count)
            expect(parsed_response["kills_by_means"].count).to eql(@game.cause_of_deaths.count)
            parsed_response["players"].each do |player|
                expect(@game.players.where(id: player["id"])).not_to be_nil
            end
            parsed_response["kills_by_means"].each do |kills|
                expect(@game.cause_of_deaths.where(id: kills["id"])).not_to be_nil
            end
        end

    end
 
end
