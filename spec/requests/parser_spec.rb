require 'rails_helper'
 
RSpec.describe "Parser", type: :request do
    
    describe "POST /create" do
        before do
            post "/parsers", params: { parser: {log: Rack::Test::UploadedFile.new("#{Rails.root}/public/qgames.log", 'qgames/log')} }
        end

        it "returns found" do
            expect(response.status).to eql(302)
        end

        context "analyzing the uploaded log file in public path" do
            
            it "105 kills in the game with 20 by <world>" do
                amount = Game.last.kills
                expect(amount).to eql(105)
            end

            it "105-40 deaths points occurred between players" do
                amount = 0
                Game.last.player_in_games.each do |player|
                    amount += player.kills
                end
                expect(amount).to eql(65)
            end

            it "4 players played in game" do
                players = ["Isgalamido","Dono da Bola","Zeh","Assasinu Credi"]
                expect(Game.last.player_in_games.count).to eql(players.count)
                Game.last.player_in_games.each do |obj|
                    expect(players.include? obj.player.name).to eql(true)
                end
            end

            it "players died from 7 differents causes in game" do
                deaths = ["MOD_TRIGGER_HURT","MOD_RAILGUN","MOD_ROCKET_SPLASH","MOD_FALLING","MOD_MACHINEGUN","MOD_ROCKET","MOD_SHOTGUN"]
                expect(Game.last.cause_of_deaths.count).to eql(deaths.count)
                Game.last.cause_of_deaths.each do |obj|
                    expect(deaths.include? obj.cause_of_death_by).to eql(true)
                end
            end

            it "player Isgalamido made 19 deaths points" do
                player = Player.where(name: "Isgalamido").first
                expect(player.name).to eql("Isgalamido")
                game = Game.last.player_in_games.where(player_id: player.id).first
                expect(game.kills).to eql(19)
            end

            it "player Zeh made 20 deaths points and win the game" do
                player = Player.where(name: "Zeh").first
                expect(player.name).to eql("Zeh")
                game = Game.last.player_in_games.where(player_id: player.id).first
                expect(game.kills).to eql(20)
                winner_id = Game.last.player_in_games.order(kills: :desc).first.player_id
                expect(winner_id).to eql(player.id)
            end

            it "the number of each cause of death" do
                deaths = [{name: "MOD_TRIGGER_HURT", count: 9},
                    {name: "MOD_RAILGUN", count: 8},{name: "MOD_ROCKET_SPLASH", count: 51},
                    {name: "MOD_FALLING", count: 11},{name: "MOD_MACHINEGUN", count: 4},
                    {name: "MOD_ROCKET", count: 20},{name: "MOD_SHOTGUN", count: 2}]

                deaths.each do |death|
                    cause_of_death = Game.last.cause_of_deaths.where(cause_of_death_by: death[:name]).first
                    expect(cause_of_death.cause_of_death_by).to eql(death[:name])
                    expect(cause_of_death.count).to eql(death[:count])
                end                
            end

        end

    end
 
end
