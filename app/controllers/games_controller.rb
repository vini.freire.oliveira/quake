class GamesController < ApplicationController
    before_action :set_game, only: [:show]

    def index
        @games = Game.all.order(kills: :desc)
        respond_to do |format|
            format.html
            format.json { render json: @games }
        end
    end

    def show
        respond_to do |format|
            format.html
            format.json { render json: @game }
        end
    end

    private

    def set_game
        @game = Game.find(params[:id])
    end
end
