class ParsersController < ApplicationController
    before_action :parse_file, only: [:create]

    def create 
        if Game.all.count > 0
            redirect_to root_path, notice: 'The file was successfully uploaded'
        else
            redirect_to root_path, notice: 'Something unexpected happened'
        end
    end

    private

    def parser_params
        params.require(:parser).permit(:log)
    end

    def parse_file
        ParserService.new(parser_params[:log]).perform
    end
end
