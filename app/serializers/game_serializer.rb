class GameSerializer < ActiveModel::Serializer
  attributes :id, :kills, :players, :player_kills, :kills_by_means

  def players
    object.players.select(:id, :name).order(name: :asc)
  end

  def player_kills
    attributes = []
    object.player_in_games.order(kills: :desc).each do |game|
      attributes.push({game.player.name => game.kills})
    end
    attributes
  end

  def kills_by_means
    attributes = []
    object.cause_of_deaths.order(count: :desc).each do |obj|
      attributes.push({obj.cause_of_death_by => obj.count})
    end
    attributes
  end
end
