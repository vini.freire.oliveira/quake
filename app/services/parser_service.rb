class ParserService

    LOG_FORMAT = /((0[0-9]|1[0-9]|2[0-3]|[0-9]):[0-5][0-9]) (ShutdownGame:|Kill:)/
    MSG_FORMAT = /[0-9]|:|killed|by/
    Log = Struct.new(:date, :type, :killer, :murdered, :cause_by)

    def initialize file
        @path = file.path
    end

    def perform
        parse_file
    end

    private

    def parse_file
        logs = []
        File.open(@path,'r').each do |line| 
            next unless line.match(LOG_FORMAT)
            log = line.split(LOG_FORMAT).reject(&:blank?)
            if log[2] == "Kill:"
                msg = log[3].split(MSG_FORMAT).reject(&:blank?)
                logs.push(Log.new( log[0], clear_str(log[2]), clear_str(msg[0]), clear_str(msg[1]), clear_str(msg[2]) ))
            elsif log[2] == "ShutdownGame:"
                save_game(logs) if logs.count > 0
                logs.clear
            end
        end
    end

    def clear_str(str)
        str.lstrip.chop
    end

    def save_game(logs)
        game = Game.create(kills: logs.count)
        logs.each do |log|
            if log.killer == "<world>"
                murdered = Player.find_or_create_by(name: log.murdered)
                player_in_game = PlayerInGame.where(game_id: game.id, player_id: murdered.id).first_or_create
                player_in_game.kills -= 1
                player_in_game.save
            else
                killer = Player.find_or_create_by(name: log.killer)
                murdered = Player.find_or_create_by(name: log.murdered)
                player_in_game = PlayerInGame.where(game_id: game.id, player_id: killer.id).first_or_create
                player_in_game.kills += 1
                player_in_game.save
            end
            cause_of_deaths = CauseOfDeath.where(game_id: game.id, cause_of_death_by: log.cause_by).first_or_create
            cause_of_deaths.count += 1
            cause_of_deaths.save
        end        
    end

end
