class Game < ApplicationRecord
    has_many :player_in_games, dependent: :destroy
    has_many :cause_of_deaths, dependent: :destroy

    has_many :players, through: :player_in_games

end
