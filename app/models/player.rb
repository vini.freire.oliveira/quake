class Player < ApplicationRecord
    has_many :player_in_games, dependent: :destroy

    validates :name, presence: true, uniqueness: true
end
