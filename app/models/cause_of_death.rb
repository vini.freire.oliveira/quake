class CauseOfDeath < ApplicationRecord
  belongs_to :game

  validates_presence_of :cause_of_death_by

end
