## Quake

#####  About

This app was developed as part of the hiring process for CloudWalk Ruby Developer position.

#####  System dependencies

- Docker (<https://www.docker.com/>)
- Docker-compose

#####  Gems

- gem 'brakeman' brakeman is a security scanner for Ruby on Rails applications.
  - The brakeman results is in docs path as brakeman-report.html 
- gem 'active_model_serializers' for object serialization
- The following gems were used to help in the automated tests
  - rspec
  - capybara
  - ffaker
  - factory_bot_rails
  - The Rspec results is in docs path as rspec-result.html

#####  Step by step to run the project in localhost using docker-compose

1. Clone the project. The project is on git in private account.

   ```
   git clone https://gitlab.com/vini.freire.oliveira/quake.git
   ```

2. Build the containers and install all dependencies

   ```
   docker-compose run --rm app bundle install
   ```

3. Add jquery lib

   ```
   docker-compose run --rm app bundle exec yarn add jquery
   ```

4. Add fontawesome styles

   ```
   docker-compose run --rm app bundle exec yarn add @fortawesome/fontawesome-free
   ```

5. Now, create database and generate migrations:

   ```
   docker-compose run --rm app bundle exec rails db:create db:migrate
   ```

6. Start the server with

   ```
   docker-compose up
   ```

7. Open browser in Localhost:3000. 

   1. <a href="https://gist.githubusercontent.com/scalone/ad679e38ca70ebf02473/raw/15887f45b3188dbda7e3bce279e4d53d50b92e9b/qgames.log"> Download the qgames.log file</a>
   2. On navbar, click on button and upload the file. 
   3.  To return json:
      1. Localhost:3000/games.json
      2. Localhost:3000/games/#{game.id}.json

#####  To run Rspec test results

1. To run the requests spec

   ```
   docker-compose run --rm app bundle exec rspec spec/requests/
   ```

2. To run all spec files

   ```
   docker-compose run --rm app bundle exec rspec spec
   ```

3. To run and save all spec files

   ```
   docker-compose run --rm app bundle exec rspec spec/ --format h > docs/rspec-result.html
   ```

#####  To run Breakman report results

1. To generate the report

   ```
   docker-compose run --rm app bundle exec brakeman -f html >> docs/brakeman-report.html
   ```

###  Links

#####  [My Git](https://gitlab.com/vini.freire.oliveira) 

#####  [My LikedIn](https://www.linkedin.com/in/vinicius-freire-b53507107/)