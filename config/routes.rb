Rails.application.routes.draw do
  root to: "games#index"
  resources :parsers, only: [:create]
  resources :games, only: [:index, :show]
end
